import features from './config';

export default featureName => features[featureName];
