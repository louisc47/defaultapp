import React from 'react';
import { Provider } from 'react-redux';
import { createAppContainer } from 'react-navigation';
import MainNavigator from './app/navigator/main';
import store from './app/redux/main';

const AppContainer = createAppContainer(MainNavigator);

export default function App() {
  return (
    <Provider store={store()}>
      <AppContainer />
    </Provider>
  );
}
