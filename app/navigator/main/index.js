import { createStackNavigator } from 'react-navigation-stack';
import routes from './routes';
import config from './config';

export default createStackNavigator(routes, config);
