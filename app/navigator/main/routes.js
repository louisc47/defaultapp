import HomeScreen from '../../screens/Home';
import KEYS from '../keys';
import withHeader from '../../hoc/withHeader';

const routes = {};

routes[KEYS.HOMESCREEN] = { screen: withHeader(HomeScreen) };

export default routes;
