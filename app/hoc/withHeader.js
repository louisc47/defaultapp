import React from 'react';
import { View } from 'react-native';
import Header from '../components/Header';

export default WrappedComponent => {
  class withHeader extends React.Component {
    render() {
      <View>
        <Header />
        <WrappedComponent />
      </View>;
    }
  }
  return withHeader;
};
