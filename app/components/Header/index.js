import React from 'react';
import { Header, Left, Body, Right, Title } from 'native-base';
import { withNavigation } from 'react-navigation';

class NavHeader extends React.Component {
  render() {
    return (
      <Header>
        <Body>
          <Title>DefaultApp</Title>
        </Body>
      </Header>
    );
  }
}

export default withNavigation(NavHeader);
