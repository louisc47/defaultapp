import { fork } from 'redux-saga/effects';
import application from './application/saga';

const sagas = [application];

export default function* rootSaga() {
  yield sagas.map(saga => fork(saga));
}
