export const SET_APPLICATION_VERSION = 'SET_APPLICATION_VERSION';
export const SET_APPLICATION_DEVICE = 'SET_APPLICATION_DEVICE';
export const SHARE_APPLICATION_VERSION = 'SHARE_APPLICATION_VERSION';
export const SHARE_APPLICATION_DEVICE = 'SHARE_APPLICATION_DEVICE';

export const setApplicationVersion = (version = 'not defined') => ({
  type: SET_APPLICATION_VERSION,
  data: version,
});

export const setApplicationDevice = (device = 'not defined') => ({
  type: SET_APPLICATION_DEVICE,
  data: device,
});
