const initialState = {
  version: null,
  device: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
