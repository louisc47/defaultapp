import { takeEvery } from 'redux-saga/effects';
import { SET_APPLICATION_VERSION } from './action';

function* helloSaga() {
  console.log('Hello Sagas');
}

export default function*() {
  yield takeEvery(SET_APPLICATION_VERSION, helloSaga);
}
